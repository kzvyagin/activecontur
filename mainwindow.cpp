#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "activecontur.h"
#include <QImage>
#include <QRgb>
#include <QLabel>
#include <QPixmap>
#include <QPoint>
#include <QList>
#include <QDebug>

#include <QPainter>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    qDebug()<<"privet"<<9;



    data=Examples::simpleGradientExample();


    // посмотреть с увеличением


    QImage sourceCopy=data.source;
    QPainter qPainter(&sourceCopy);
    qPainter.setBrush(Qt::NoBrush);
    qPainter.setPen(Qt::green);
    qPainter.drawPath( data.getPolygoneAsPath( data.polygone ) );
    bool bEnd = qPainter.end();

    ui->label->setPixmap( QPixmap::fromImage(sourceCopy.scaled( QSize(400,400) )) );
    ui->label->show();


}

MainWindow::~MainWindow()
{
    delete ui;
}

QImage MainWindow::generateSimpleExample()
{
    QImage img(7,7,QImage::Format_RGB32);
    for(int  x=0; x<7 ; x++)
    {
        for(int y=0; y<7 ; y++)
        {
            img.setPixel(x,y, QColor(255,255,255).rgb() );
        }
    }

    img.setPixel(3,3, QColor(0,0,0).rgb() );

    return img;
}

void MainWindow::on_pushButton_clicked()
{
    static int stepDepth=1;
    //функция расчета энергии на одном пикселе
    Contur::DefaultPredicate df(data.source);



     QList<QPoint> resultContur=Contur::activeContur(data.source,
                                                     data.polygone,
                                                     df ,
                                                     data.coefficents
                                                     ,5,0,stepDepth);

     stepDepth=stepDepth+1;
     qDebug()<<"resultContur"<<resultContur<<stepDepth;

     QImage sourceCopy=data.source;
     QPainter qPainter(&sourceCopy);
     qPainter.setBrush(Qt::NoBrush);
     qPainter.setPen(Qt::green);
     qPainter.drawPath( data.getPolygoneAsPath(resultContur) );
     bool bEnd = qPainter.end();

     ui->label->setPixmap( QPixmap::fromImage(sourceCopy.scaled(QSize(400,400))) );
     ui->label->show();

}
