#include "activecontur.h"
#include <QSet>
#include <QDebug>
#include <QList>
#include <stdlib.h>
#include <cmath>
#include <QApplication>
using namespace std;

namespace  Contur
{

QList<QPoint> activeContur(const QImage &img,
                            const QList<QPoint> &polygonePoints,
                            PredicateInterfase &prepicate,
                            QList<double> coefficents ,
                            int operatorSize,
                            int minOrMax,
                            int debugMaxStepDepth)
{

    struct Energy
    {
        int x;
        int y;
        QList<double> energies;
        double energy;
        Energy(int x,
               int y,
               QList<double> energies,
               QList<double> coefficents = QList<double>() ):
            x(x),
            y(y),
            energies(energies),
            energy(energies.value(0,0) )
        {
            if(      coefficents.size() >0
                  && energies.size()== coefficents.size() )
            {
                energy = 0;
                for(int i=0; i< energies.size(); i++)
                {
                    energy+=energies[i]*coefficents[i];
                }
            }
            else
            {
                energy=0;
                for(int i=0; i< energies.size(); i++)
                {
                    energy+=energies[i] ;
                }
            }
        }
        void print()
        {
            qDebug()<<"Energy"<<x<<y<<energy<<energies;
        }
    };

    int sizeOfWindow=operatorSize;

    int trashhold=polygonePoints.size()/4;

    int moved=0;

    int oldMoved=0;

    int countSameSteps=10;
    QList<QPoint> points=polygonePoints;
    do
    {

        oldMoved=moved;
        moved=0;
        for(int i=0; i<points.size() ; i++)
        {
            QPoint &point=points[i];
            qDebug()<<"for point"<<point;
            int minX=point.x()-(sizeOfWindow-1)/2;
            int maxX=point.x()+(sizeOfWindow-1)/2;
            int minY=point.y()-(sizeOfWindow-1)/2;
            int maxY=point.y()+(sizeOfWindow-1)/2;

            if( minX > maxX ) {
               qSwap(minX,maxX);
            }

            if( minY > maxY ) {
               qSwap(minY,maxY);
            }

            minX=minX < 0 ? 0 : minX;
            maxX= ( maxX >= img.width() ) ? img.width()-1 : maxX;

            minY=minY < 0 ? 0 : minY;
            maxY= ( maxY >= img.height() ) ? img.height()-1 : maxY;

            QRect rect(minX,minY,maxX-minX,maxY-minY);

            QList <Energy> eList;
            Energy optimumEnergy(0,0,QList<double>()<<99999999999);
            if(minOrMax==1)
            {
                optimumEnergy= Energy(0,0,QList<double>()<<0);
            }

            for( int x=minX; x <= maxX ; x++ )
            {
                for( int y=minY; y <= maxY ; y++ )
                {
                     Energy curEnergy(x, y, prepicate( point,QPoint(x, y),rect ), coefficents );
                     curEnergy.print();
                     eList <<  curEnergy;

                    if( optimumEnergy.energy > curEnergy.energy )
                    {
                        optimumEnergy=curEnergy;
                    }

                    if( minOrMax==1 )
                    {
                        if( optimumEnergy.energy < curEnergy.energy )
                        {
                            optimumEnergy=curEnergy;
                        }
                    }
                }
            }
             qDebug()<<"minenergy:";optimumEnergy.print();
            //принятие решение о передвижении
            if(    optimumEnergy.x !=point.x()
                || optimumEnergy.y !=point.y() )
            {
                point.setX( optimumEnergy.x );
                point.setY( optimumEnergy.y );
                moved++;
            }
           // double calculatedEnergy=alpha *1 + beta*1 +gamma *1;

            debugMaxStepDepth=debugMaxStepDepth-1;
            if(debugMaxStepDepth==0)
            {
                 return points;
            }

        }
         qDebug()<<"moved="<<moved<<"trashhold"<<trashhold<<"oldMoved"<<oldMoved<<"countSameSteps"<<countSameSteps;
         if( oldMoved == moved  )
         {
             countSameSteps--;
         }
         if(countSameSteps==0)
         {
             break;
         }
    } while(    ( moved > trashhold )  );


    return points;
}

DefaultPredicate::DefaultPredicate(const QImage &img):
    img( img )
{

}

QList<double> DefaultPredicate::operator ()( QPoint rootPoint, QPoint pointTo, QRect rect )
{
    Q_UNUSED(rect);
    double distance = sqrt( pow( pointTo.x()-rootPoint.x(),2)+ pow(pointTo.y()-rootPoint.y(),2) );
    QRgb pixel=img.pixel(pointTo);
    double grayIndex=qGray(pixel);

    return QList<double>()<<distance<<grayIndex;
}



}

uint qHash(const QPoint &key)
{
    return qHash (QPair<int,int>(key.x(), key.y()) );
}
