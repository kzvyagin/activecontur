#-------------------------------------------------
#
# Project created by QtCreator 2018-09-29T11:57:39
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ActiveContur
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS


SOURCES += \
        main.cpp \
        mainwindow.cpp \
        activecontur.cpp \
    imageexamples.cpp

HEADERS += \
        mainwindow.h \
        activecontur.h \
    imageexamples.h

FORMS += \
        mainwindow.ui
