#include "imageexamples.h"



ExampleData Examples::simpleExample()
{
    ExampleData data;

    QImage img(7,7,QImage::Format_RGB32);
    for(int  x=0; x<7 ; x++)
    {
        for(int y=0; y<7 ; y++)
        {
            img.setPixel(x,y, QColor(255,255,255).rgb() );
        }
    }

    img.setPixel(3,3, QColor(0,0,0).rgb() );

    data.source=img;


    //исходный, грубый контур
    QList<QPoint> polygone;
    polygone<<QPoint(2,2)<<QPoint(2,4)<<QPoint(4,4)<<QPoint(4,2);

    data.polygone=polygone;

    //коэффиценты для расчета энергии
    QList<double> coefficents;
    coefficents<<1<<1;
    data.coefficents=coefficents;

    return data;
}

ExampleData Examples::simpleExampleBig()
{
    ExampleData data;

    QImage img(14,14,QImage::Format_RGB32);
    for(int  x=0; x<14 ; x++)
    {
        for(int y=0; y<14 ; y++)
        {
            img.setPixel(x,y, QColor(255,255,255).rgb() );
        }
    }

    for(int x=3;x<6;x++)
    {
        for(int y=3;y<6;y++)
        {
            img.setPixel(x,y, QColor(0,0,0).rgb() );
        }
    }


    data.source=img;


    //исходный, грубый контур
    QList<QPoint> polygone;
    polygone<<QPoint(1,1)<<QPoint(1,7)<<QPoint(7,7)<<QPoint(7,1);

    data.polygone=polygone;

    //коэффиценты для расчета энергии
    QList<double> coefficents;
    coefficents<<1<<1;
    data.coefficents=coefficents;

    return data;
}

QPainterPath ExampleData::getPolygoneAsPath(QList<QPoint > points)
{
     QPainterPath path;
     path.moveTo(points.value(0));
     foreach (QPoint p, points)
     {
        path.lineTo(p);
     }
     path.lineTo(points.value(0));
     return path;
}

ExampleData Examples::simpleGradientExample()
{
    ExampleData data;

    QImage img(24,24,QImage::Format_RGB32);
    for(int  x=0; x<24 ; x++)
    {
        for(int y=0; y<24 ; y++)
        {
            img.setPixel(x,y, QColor(255,255,255).rgb() );
        }
    }

    for(int x=3;x<14;x++)
    {
        for(int y=3;y<14;y++)
        {
            img.setPixel(x,y, QColor(x*15,x*15,x*15).rgb() );
        }
    }


    data.source=img;


    //исходный, грубый контур
    QList<QPoint> polygone;
    polygone<<QPoint(1,1)<<QPoint(1,15)<<QPoint(15,15)<<QPoint(15,1);

    data.polygone=polygone;

    //коэффиценты для расчета энергии
    QList<double> coefficents;
    coefficents<<0.01<<1;
    data.coefficents=coefficents;

    return data;
}
