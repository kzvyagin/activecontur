#ifndef IMAGEEXAMPLES_H
#define IMAGEEXAMPLES_H
#include <QImage>
#include <QList>
#include <QPoint>
#include <QPolygon>

struct ExampleData
{
    QImage source;
    QList<QPoint> polygone;
    QList<double> coefficents;
    QPainterPath getPolygoneAsPath(QList<QPoint> points);
};
namespace Examples
{
   ExampleData simpleExample();
   ExampleData simpleExampleBig();
   ExampleData simpleGradientExample();
}

#endif // IMAGEEXAMPLES_H
