#ifndef CONTUR_H
#define CONTUR_H
#include <QPolygon>
#include <QImage>
#include <QList>
#include <QPoint>
#include <QRect>

namespace  Contur
{
    struct PredicateInterfase
    {
        PredicateInterfase(){}
        virtual QList<double> operator ()( QPoint rootPoint, QPoint pointTo, QRect rect )=0;
    };

    struct DefaultPredicate :public PredicateInterfase
    {
        const QImage &img;

        DefaultPredicate( const QImage &img );

         QList<double> operator ()( QPoint rootPoint, QPoint pointTo, QRect rect ) override;
    };


    ///
    /// \brief activeContur
    /// \param img
    /// \param polygonePoints
    /// \param prepicate
    /// \param coefficents
    /// \param operatorSize
    /// \param minOrMax 0- min optimum 1 - max optimum
    /// \param debugMaxStepDepth
    /// \return
    ///
    QList<QPoint> activeContur(const QImage &img,
                      const QList< QPoint > &polygonePoints,
                      PredicateInterfase &prepicate,
                      QList<double> coefficents,
                      int operatorSize ,
                      int minOrMax=0,
                      int debugMaxStepDepth=0);


}
    inline uint qHash (const QPoint & key);
#endif // CONTUR_H
